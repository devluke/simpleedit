package net.lukepchambers.simpleedit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.lukepchambers.simpleedit.cmd.ChunkCmd;
import net.lukepchambers.simpleedit.cmd.HposCmd;
import net.lukepchambers.simpleedit.cmd.PosCmd;
import net.lukepchambers.simpleedit.cmd.ReplaceCmd;
import net.lukepchambers.simpleedit.cmd.SetCmd;
import net.lukepchambers.simpleedit.cmd.UndoCmd;
import net.lukepchambers.simpleedit.cmd.WandCmd;

public class SimpleEdit extends JavaPlugin implements Listener {
	
	public Logger log;
	
	private Util util;
	
	public Map<String, Location> pos1 = new HashMap<String, Location>();
	public Map<String, Location> pos2 = new HashMap<String, Location>();
	
	public Map<String, List<List<Location>>> undo = new HashMap<String, List<List<Location>>>();
	
	@Override
	public void onEnable() {
		
		log = getLogger();
		
		util = new Util(this);
		
		getServer().getPluginManager().registerEvents(this, this);
		
		getCommand("/chunk").setExecutor(new ChunkCmd(this));
		getCommand("/hpos1").setExecutor(new HposCmd(this));
		getCommand("/hpos2").setExecutor(new HposCmd(this));
		getCommand("/pos1").setExecutor(new PosCmd(this));
		getCommand("/pos2").setExecutor(new PosCmd(this));
		getCommand("/replace").setExecutor(new ReplaceCmd(this));
		getCommand("/set").setExecutor(new SetCmd(this));
		getCommand("/undo").setExecutor(new UndoCmd(this));
		getCommand("/wand").setExecutor(new WandCmd());
		
		log.info("simpleedit enabled");
		
	}
	
	@Override
	public void onDisable() {
		
		log.info("simpleedit disabled");
		
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		
		String name = e.getPlayer().getName();
		
		undo.put(name, new ArrayList<List<Location>>());
		
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		String name = e.getPlayer().getName();
		
		pos1.remove(name);
		pos2.remove(name);
		
		undo.remove(name);
		
	}
	
	@EventHandler
	public void onPlayerBreakBlock(BlockBreakEvent e) {
		
		Player player = e.getPlayer();
		
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		
		if (mainHand.getType().equals(Material.WOODEN_AXE)) {
			
			e.setCancelled(true);
			
			util.setPos(player, e.getBlock().getLocation(), 1);
			
			player.sendMessage("pos1 set");
			
		}
		
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			
			Player player = e.getPlayer();
			
			if (e.getHand().equals(EquipmentSlot.HAND)) {
			
				ItemStack mainHand = player.getInventory().getItemInMainHand();
			
				if (mainHand.getType().equals(Material.WOODEN_AXE)) {
				
					e.setCancelled(true);
				
					util.setPos(player, e.getClickedBlock().getLocation(), 2);
					
					player.sendMessage("pos2 set");
				
				}
			
			}
			
		}
		
	}

}