package net.lukepchambers.simpleedit.cmd;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.lukepchambers.simpleedit.SimpleEdit;
import net.lukepchambers.simpleedit.Util;

public class HposCmd implements CommandExecutor {

	private Util util;
	
	public HposCmd(SimpleEdit pl) {
		
		util = new Util(pl);
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!((cmd.getName().equals("/hpos1")) || (cmd.getName().equals("/hpos2")))) {
			
			return false;
			
		}
		
		if (!(sender instanceof Player)) {
			
			sender.sendMessage("must be player");
			
			return true;
			
		}
		
		Player player = (Player) sender;
		
		if (args.length != 0) {
			
			sender.sendMessage("invalid args");
			
			return true;
			
		}
		
		int pos = (cmd.getName().equals("/hpos1")) ? 1 : 2;
		
		util.setPos(player, player.getTargetBlock(null, 1000).getLocation(), pos);
		
		player.sendMessage("pos" + pos + " set");
		
		return true;
		
	}
	
}