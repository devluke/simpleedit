package net.lukepchambers.simpleedit.cmd;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class WandCmd implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!cmd.getName().equals("/wand")) {
			
			return false;
			
		}
		
		if (!(sender instanceof Player)) {
			
			sender.sendMessage("must be player");
			
			return true;
			
		}
		
		Player player = (Player) sender;
		
		if (args.length != 0) {
			
			player.sendMessage("invalid args");
			
			return false;
			
		}
		
		ItemStack oldItem = player.getInventory().getItemInMainHand();
		
		player.getInventory().setItemInMainHand(new ItemStack(Material.WOODEN_AXE));
		player.getInventory().addItem(oldItem);
		
		player.sendMessage("left click for pos1, right click for pos2");
		
		return true;
		
	}

}