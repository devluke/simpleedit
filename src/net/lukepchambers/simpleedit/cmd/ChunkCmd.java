package net.lukepchambers.simpleedit.cmd;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.lukepchambers.simpleedit.SimpleEdit;
import net.lukepchambers.simpleedit.Util;

public class ChunkCmd implements CommandExecutor {
	
	private Util util;
	
	public ChunkCmd(SimpleEdit pl) {
		
		util = new Util(pl);
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!cmd.getName().equals("/chunk")) {
			
			return false;
			
		}
		
		if (!(sender instanceof Player)) {
			
			sender.sendMessage("must be player");
			
			return true;
			
		}
		
		Player player = (Player) sender;
		
		Chunk chunk = player.getWorld().getChunkAt(player.getLocation());
		
		Location pos1 = chunk.getBlock(0, 0, 0).getLocation();
		Location pos2 = chunk.getBlock(15, 255, 15).getLocation();
		
		util.setPos(player, pos1, 1);
		util.setPos(player, pos2, 2);
		
		player.sendMessage("selected chunk");
		
		return true;
		
	}

}