package net.lukepchambers.simpleedit.cmd;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.lukepchambers.simpleedit.BlockPattern;
import net.lukepchambers.simpleedit.SimpleEdit;
import net.lukepchambers.simpleedit.Util;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class UndoCmd implements CommandExecutor {
	
	private SimpleEdit pl;
	
	private Util util;
	
	public UndoCmd(SimpleEdit pl) {
		
		this.pl = pl;
		
		util = new Util(pl);
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!cmd.getName().equals("/undo")) {
			
			return false;
			
		}
		
		if (!(sender instanceof Player)) {
			
			sender.sendMessage("must be player");
			
			return true;
			
		}
		
		Player player = (Player) sender;
		
		if (args.length != 0) {
			
			player.sendMessage("invalid args");
			
			return false;
			
		}
		
		List<List<Location>> undoLists = pl.undo.get(player.getName());
		
		if (undoLists.isEmpty()) {
			
			player.sendMessage("nothing left to undo");
			
			return true;
			
		}
		
		List<Location> locations = undoLists.get(undoLists.size() - 1);
		List<Block> blocks = new ArrayList<Block>();
		List<Material> materials = new ArrayList<Material>();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				for (Location location : locations) {
					
					blocks.add(location.getBlock());
					materials.add(location.getBlock().getType());
					
				}
				
			}
			
		}.runTaskAsynchronously(pl);
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				if (locations.size() != materials.size()) {
					
					player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("preparing operation (stage 1/2)"));
					
					return;
					
				}
				
				this.cancel();
				
				System.out.println(locations);
				System.out.println(blocks);
				System.out.println(materials);
				
				undoLists.remove(undoLists.size() - 1);
				
				pl.undo.replace(player.getName(), undoLists);
				
				BlockPattern pattern = new BlockPattern(pl, blocks.size(), materials);
				
				util.setBlockList(blocks, pattern, player, true);
				
				player.sendMessage("successfully handed undo to new operation");
				
			}
			
		}.runTaskTimer(pl, 0, 1);
		
		return true;
		
	}

}