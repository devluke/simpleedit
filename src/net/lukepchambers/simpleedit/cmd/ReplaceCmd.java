package net.lukepchambers.simpleedit.cmd;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.lukepchambers.simpleedit.BlockPattern;
import net.lukepchambers.simpleedit.SimpleEdit;
import net.lukepchambers.simpleedit.Util;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class ReplaceCmd implements CommandExecutor {
	
	private SimpleEdit pl;
	
	private Util util;
	
	public ReplaceCmd(SimpleEdit pl) {
		
		this.pl = pl;
		
		util = new Util(pl);
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!cmd.getName().equals("/replace")) {
			
			return false;
			
		}
		
		if (!(sender instanceof Player)) {
			
			sender.sendMessage("must be player");
			
			return true;
			
		}
		
		Player player = (Player) sender;
		
		if (args.length != 2) {
			
			player.sendMessage("invalid args");
			
			return false;
			
		}
		
		if (!(pl.pos1.containsKey(player.getName()) && pl.pos2.containsKey(player.getName()))) {
			
			player.sendMessage("must set pos1 and pos2");
			
			return true;
			
		}
		
		Material replaceBlock = Material.getMaterial(args[0].toUpperCase());
		
		if ((replaceBlock == null) || !replaceBlock.isBlock()) {
			
			player.sendMessage("invalid replace block");
			
			return true;
			
		}
		
		Material setBlock = Material.getMaterial(args[1].toUpperCase());
		
		if ((setBlock == null) || !setBlock.isBlock()) {
			
			player.sendMessage("invalid set block");
			
			return true;
			
		}
		
		Location pos1 = pl.pos1.get(player.getName());
		Location pos2 = pl.pos2.get(player.getName());
		
		List<Block> blocks = new ArrayList<Block>();
		
		List<BlockPattern> pattern = new ArrayList<BlockPattern>();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				List<Block> allBlocks = util.getBlocksBetweenTwoPoints(pos1, pos2);
				List<Block> filteredBlocks = allBlocks.stream().filter(block -> block.getType().equals(replaceBlock)).collect(Collectors.toList());
				
				for (Block block : filteredBlocks) {
					
					blocks.add(block);
					
				}
				
				pattern.add(new BlockPattern(pl, blocks.size(), setBlock));
				
			}
			
		}.runTaskAsynchronously(pl);
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				if (pattern.isEmpty()) {
					
					player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("preparing operation (stage 1/2)"));
					
					return;
					
				}
				
				this.cancel();
				
				util.setBlockList(blocks, pattern.get(0), player);
				
				player.sendMessage("successfully handed replace to new operation");
				
			}
			
		}.runTaskTimer(pl, 0, 1);
		
		return true;
		
	}

}