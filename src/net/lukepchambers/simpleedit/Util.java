package net.lukepchambers.simpleedit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Util {
	
	private SimpleEdit pl;
	
	public Util(SimpleEdit pl) {
		
		this.pl = pl;
		
	}

	public List<Block> getBlocksBetweenTwoPoints(Location loc1, Location loc2) {
		
		List<Block> blocks = new ArrayList<Block>();

		int topBlockX = (loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
		int bottomBlockX = (loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());

		int topBlockY = (loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
		int bottomBlockY = (loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());

		int topBlockZ = (loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
		int bottomBlockZ = (loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());

		for (int x = bottomBlockX; x <= topBlockX; x++) {

			for (int z = bottomBlockZ; z <= topBlockZ; z++) {

				for (int y = bottomBlockY; y <= topBlockY; y++) {

					Block block = loc1.getWorld().getBlockAt(x, y, z);

					blocks.add(block);

				}

			}

		}
		
		return blocks;

	}
	
	public void setPos(Player player, Location location, int pos) {
		
		(pos == 1 ? pl.pos1 : pl.pos2).put(player.getName(), location);
		
	}
	
	public void setBlockList(List<Block> blocks, BlockPattern pattern, Player player) {
		
		this.setBlockList(blocks, pattern, player, false);
		
	}
	
	public void setBlockList(List<Block> blocks, BlockPattern pattern, Player player, boolean undo) {
		
		if (!undo) {
		
			List<List<Location>> undoList = pl.undo.get(player.getName());
			
			List<Location> locations = new ArrayList<Location>();
			
			for (Block block : blocks) {
				
				locations.add(block.getLocation());
				
			}
		
			undoList.add(locations);
		
			pl.undo.replace(player.getName(), undoList);
			
		}
		
		List<List<Block>> blockLists = new ArrayList<List<Block>>();
		
		long startTime = System.currentTimeMillis();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				for (Block block : blocks) {
					
					int lastIndex = blockLists.size() - 1;
					
					if (blockLists.isEmpty() || (blockLists.get(lastIndex).size() == 2000)) {
						
						blockLists.add(Arrays.asList(block));
						
					} else {
						
						List<Block> newBlockList = new ArrayList<Block>(blockLists.get(lastIndex));
						
						newBlockList.add(block);
						
						blockLists.set(lastIndex, newBlockList);
						
					}
					
				}
				
			}
			
		}.runTaskAsynchronously(pl);
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				
				if ((blockLists.size() < Math.ceil((((double) blocks.size()) / 2000))) || !pattern.done) {
					
					player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("preparing operation (stage 2/2)"));
					
					return;
					
				}
				
				this.cancel();
				
				double totalSize = blockLists.size();
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						
						if (blockLists.isEmpty()) {
							
							long endTime = System.currentTimeMillis();
							
							this.cancel();
							
							int secondsTook = (int) ((endTime - startTime) / 1000);
							
							player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("operation complete, took " + secondsTook + " seconds"));
							
							return;
							
						}
						
						List<Block> currentList = blockLists.get(0);
						
						for (Block block : currentList) {
							
							block.setType(pattern.pattern.get(currentList.indexOf(block)));
							
						}
						
						blockLists.remove(0);
						
						double newSize = blockLists.size();
						
						int percentDone = (int) (((totalSize - newSize) / totalSize) * 100);
						
						int estimatedTime = (int) (newSize / 20);
						
						player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("executing operation, " + percentDone + "% done; should finish in ~" + estimatedTime + " seconds"));
						
					}
					
				}.runTaskTimer(pl, 0, 1);
				
			}
			
		}.runTaskTimer(pl, 0, 1);
		
	}

}