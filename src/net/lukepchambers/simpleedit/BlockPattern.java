package net.lukepchambers.simpleedit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Material;

public class BlockPattern {

	public List<Material> pattern = new ArrayList<Material>();

	public boolean done = false;

	public BlockPattern(SimpleEdit pl, int size, Material block) {

		this(pl, size, Arrays.asList(block), false);

	}

	public BlockPattern(SimpleEdit pl, int size, List<Material> blocks) {

		this(pl, size, blocks, false);

	}

	public BlockPattern(SimpleEdit pl, int size, List<Material> blocks, boolean random) {

		List<Integer> repeatBlockIndexList = new ArrayList<Integer>();

		for (int i = 0; i < (size / blocks.size()); i++) {

			for (int i2 = 0; i2 < blocks.size(); i2++) {

				repeatBlockIndexList.add(i2);

			}

		}

		for (int i = 0; i < (size % blocks.size()); i++) {

			repeatBlockIndexList.add(i);

		}

		for (int i = 0; i < size; i++) {

			if (random) {

				Random rand = new Random();

				Material randomBlock = blocks.get(rand.nextInt(blocks.size()));

				pattern.add(randomBlock);

			} else {

				pattern.add(blocks.get(repeatBlockIndexList.get(i)));

			}

		}

		done = true;

	}

}